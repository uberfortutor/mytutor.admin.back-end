var db = require('../db');

module.exports = {
    findOne: (id) => {
        return db.load(`select * from skill where id = '${id}'`);
    },
    queryByUserId: (userId) => {
        return db.load(`select skill.* from skill join tutor_skill on skill.id = tutor_skill.skillId  where tutor_skill.tutorId = '${userId}'`);
    },
    query: (limit, offset) => {
        return db.load(`select * from skill where skill.delete = 0 limit ${limit} offset ${offset}`);
    },
    count: () => {
        return db.load(`select count(*) as total from skill where skill.delete = 0`);
    },
    add: (skill) => {
        return db.add(`skill`, skill);
    },
    udpate: (skill) => {
        return db.update(`skill`, skill, 'id');
    },
    delete: (skill) => {
        s = {
            id: skill.id,
            delete: 1,
        }
        return db.update(`skill`, s, 'id');
    }
}