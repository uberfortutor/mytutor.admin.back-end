var db = require('../db');

module.exports = {
    query: (limit, offset, searchName) => {
        return db.load(`select contract.*, u1.displayName as finderName, u2.displayName as tutorName  from (contract join user as u1 on contract.finderId = u1.id) join user as u2 on contract.tutorId = u2.id where u1.displayName like '%${searchName}%' or u2.displayName like '%${searchName}%' limit ${limit} offset ${offset}`);
    },
    count: (searchName) => {
        return db.load(`select count(*) as total from (contract join user as u1 on contract.finderId = u1.id) join user as u2 on contract.tutorId = u2.id where  u1.displayName like '%${searchName}%' or u2.displayName like '%${searchName}%'`);
    },
    queryByStatus: (limit, offset, status, searchName) => {
        return db.load(`select contract.*, u1.displayName as finderName, u2.displayName as tutorName from (contract join user as u1 on contract.finderId = u1.id) join user as u2 on contract.tutorId = u2.id where contract.status = ${status} and (u1.displayName like '%${searchName}%' or u2.displayName like '%${searchName}%') limit ${limit} offset ${offset}`);
    },
    countByStatus: (status, searchName) => {
        return db.load(`select count(*) as total from (contract join user as u1 on contract.finderId = u1.id) join user as u2 on contract.tutorId = u2.id where  contract.status = ${status} and (u1.displayName like '%${searchName}%' or u2.displayName like '%${searchName}%')`);
    },
    udpate: (contract) => {
        return db.update(`contract`, contract, 'id');
    }
}