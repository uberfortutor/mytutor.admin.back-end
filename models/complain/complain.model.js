var db = require('../db');

module.exports = {
    query: (limit, offset, searchText) => {
        return db.load(`select contract_complain.*, user.displayName as finderName from contract_complain join user on contract_complain.finderId = user.id where user.displayName like '%${searchText}%' limit ${limit} offset ${offset}`);
    },
    queryBySatus: (limit, offset, status, searchText) => {
        return db.load(`select contract_complain.*, user.displayName as finderName from contract_complain join user on contract_complain.finderId = user.id  where status=${status} and user.displayName like '%${searchText}%' limit ${limit} offset ${offset}`);
    },
    count: (searchText) => {
        return db.load(`select count(*) as total from contract_complain join user on contract_complain.finderId = user.id where user.displayName like '%${searchText}%'`);
    },
    countByStatus: (status, searchText) => {
        return db.load(`select count(*) as total from contract_complain join user on contract_complain.finderId = user.id  where status=${status} and user.displayName like '%${searchText}%'`);
    },
    udpate: (contract_complain) => {
        return db.update(`contract_complain`, contract_complain, 'id');
    },
    detail: (id) => {                                                                        
        return db.load(`select contract_complain.content, contract_complain.status, contract.* from contract_complain join contract on contract_complain.contractId = contract.id where contract_complain.id = ${id}`);
    }
}