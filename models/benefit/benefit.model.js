var db = require('../db');

module.exports = {
    queryAll: (limit, offset) => {
        return db.load(`select payment.tutorId, displayName, sum(amount) as amount, avatar, email from payment join user on payment.tutorId = user.id group by payment.tutorId, user.displayName, user.avatar, user.email order by amount desc limit ${limit} offset ${offset}`);
    },
    countAll: () => {
        return db.load(`select count(payment.tutorId) as total from payment group by payment.tutorId`);
    },
    top: (dateBegin, dateEnd, limit, offset) => {
        return db.load(`select payment.tutorId, displayName, sum(amount) as amount, avatar, email from payment join user on payment.tutorId = user.id where payment.paidDate >= '${dateBegin}' and payment.paidDate <= '${dateEnd}' group by payment.tutorId, user.displayName, user.avatar, user.email order by amount desc limit ${limit} offset ${offset}`);
    },
    countTop: (dateBegin, dateEnd) => {
        return db.load(`select count(payment.tutorId) as total from payment where payment.paidDate >= '${dateBegin}' and payment.paidDate <= '${dateEnd}' group by payment.tutorId`);
    },
    topBySkill: (dateBegin, dateEnd, limit, offset) => {
        return db.load(`select skill.id, skill.title, sum(amount) as amount from payment join (tutor_skill join skill on tutor_skill.skillId = skill.id) on payment.tutorId = tutor_skill.tutorId where payment.paidDate >= '${dateBegin}' and payment.paidDate <= '${dateEnd}' group by skill.id, skill.title order by amount desc limit ${limit} offset ${offset}`);
    },
    countTopBySkill: (dateBegin, dateEnd) => {
        return db.load(`select count(skill.id) from payment join (tutor_skill join skill on tutor_skill.skillId = skill.id) on payment.tutorId = tutor_skill.tutorId where payment.paidDate >= '${dateBegin}' and payment.paidDate <= '${dateEnd}' group by skill.id`);
    },
    topBySkillAll: (limit, offset) => {
        return db.load(`select skill.id, skill.title, sum(amount) as amount from payment join (tutor_skill join skill on tutor_skill.skillId = skill.id) on payment.tutorId = tutor_skill.tutorId group by skill.id, skill.title order by amount desc limit ${limit} offset ${offset}`);
    },
    countTopBySkillAll: () => {
        return db.load(`select count(skill.id) from payment join (tutor_skill join skill on tutor_skill.skillId = skill.id) on payment.tutorId = tutor_skill.tutorId group by skill.id`);
    }
}