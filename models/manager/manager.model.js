var db = require('../db');

module.exports = {
    findOne: (user) => {
        return db.load(`select * from manager where username = '${user.username}' and password = '${user.password}' and manager.delete = 0`);
    },
    findOneUsername: (user) => {
        return db.load(`select * from manager where username = '${user.username}' and manager.delete = 0`);
    },
    findOneId: (user) => {
        return db.load(`select * from manager where id = '${user.id}' and manager.delete = 0`);
    },
    findOneUsernameAndRole: (user, role) => {
        return db.load(`select * from manager where username = '${user.username}' and manager.delete = 0`);
    },
    query: (limit, offset) => {
        return db.load(`select * from manager where manager.delete = 0 limit ${limit} offset ${offset}`);
    },
    count: () => {
        return db.load(`select count(*) as total from manager where manager.delete = 0`);
    },
    queryByRole: (limit, offset, role) => {
        return db.load(`select * from manager where manager.delete = 0 and role = ${role} limit ${limit} offset ${offset}`);
    },
    countByRole: (role) => {
        return db.load(`select count(*) as total from manager where manager.delete = 0 and role = ${role}`);
    },
    add: (user) => {
        return db.add(`manager`, user);
    },
    update: (user) => {
        return db.update(`manager`, user, 'id');
    }
}