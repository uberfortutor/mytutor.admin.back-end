var express = require('express');
var router = express.Router();
const passport = require('passport');
const ManagerModel = require('../../models/manager/manager.model');
const bcrypt = require('bcrypt');

const saltRounds = 10;

router.get('/info', passport.authenticate('jwt', {session: false}), function (req, res, next) {
    res.json(req.user);
});

router.post('/change-pass', passport.authenticate('jwt', {session: false}), function (req, res, next) {
    var user = req.user;
    var oldPass = req.body.oldPass;
    var newPass = req.body.newPass;

    if (oldPass === undefined || oldPass === "" || newPass === undefined || newPass === "") {
        return res.status(400).json({message: "oldPass and newPass is require"});
    }

    user.password = bcrypt.hashSync(newPass, saltRounds);
    ManagerModel.findOneUsername(user).then(users => {
        if (users.length > 0) {
            if (bcrypt.compareSync(oldPass, users[0].password) === true) {
                ManagerModel.update(user).then(id => {
                    return res.status(200).json({message: "success"});
                })
                .catch(err => {
                    console.log(err);
                    return res.status(400).json({message: "failed"});
                });
            } else {
                return res.status(400).json({message: "old.pass.incorrect"})
            }
        } else {
            return res.status(400).json({message: "user.notfound"})
        }
    })
    .catch(err => {
        console.log(err);
        res.status(400).json({message: "failed"});
    })
});

module.exports = router;
