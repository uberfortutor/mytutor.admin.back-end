var express = require('express');
var router = express.Router();
const passport = require('passport');
const ContractModel = require('../../models/contract/contract.model');
const UserModel = require('../../models/user/user.model');

const limit = 10;

router.get('/list', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var page = req.query.page;
    if (page == undefined || isNaN(page)) {
        page = 1;
    }

    var lm = req.query.limit;
    if (lm == undefined || isNaN(lm)) {
        lm = limit;
    }

    var searchText = req.query.searchText;
    if (searchText == undefined) {
        searchText = "";
    }
    var status = req.query.status;

    if (!status || parseInt(status) === -1) {
        Promise.all([ContractModel.query(lm, lm * (page - 1), searchText), ContractModel.count(searchText)])
            .then(([contracts, count]) => {
                res.status(200).json({
                    contracts,
                    total: count ? count[0].total : 0,
                });
            })
            .catch(err => {
                console.log(err);
                res.status(400).json({ message: "failed" });
            })
    } else {
        Promise.all([ContractModel.queryByStatus(lm, lm * (page - 1), status, searchText), ContractModel.countByStatus(status, searchText)])
            .then(([contracts, count]) => {
                res.status(200).json({
                    contracts,
                    total: count ? count[0].total : 0,
                });
            })
            .catch(err => {
                console.log(err);
                res.status(400).json({ message: "failed" });
            })
    }
});

router.post('/cancel', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var contract = {};
    contract.id = req.body.id;
    if (contract.id == undefined) {
        return res.status(400).json({ message: "not.enough.field-data" });
    }
    contract.status = 3;
    ContractModel.udpate(contract)
        .then(id => {
            res.status(200).json({ message: "success" });
        })
        .catch(err => {
            console.log(err);
            res.status(400).json({ message: "failed" });
        });
});

module.exports = router;