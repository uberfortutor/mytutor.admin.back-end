var express = require('express');
var router = express.Router();
const passport = require('passport');
const UserModel = require('../../models/user/user.model');
const TagSkillModel = require('../../models/tagskill/tagskill.model');

const limit = 10;

router.get('/list', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var page = req.query.page;
    if (page == undefined || isNaN(page)) {
        page = 1;
    }

    var lm = req.query.limit;
    if (lm == undefined || isNaN(lm)) {
        lm = limit;
    }

    var searchName = req.query.searchName;
    if (searchName == undefined) {
        searchName = "";
    }

    var type = req.query.type;
    if (type == undefined || type == "") {
        Promise.all([UserModel.query(lm, lm * (page - 1), searchName), UserModel.count(searchName)])
            .then(([users, count]) => {
                users.map(v => {
                    delete v.password;
                })
                res.status(200).json({
                    users,
                    total: count[0] ? count[0].total : 0,
                });
            })
            .catch(err => {
                console.log(err);
                res.status(400).json({ message: "failed" });
            })
    } else {
        Promise.all([UserModel.queryByType(lm, lm * (page - 1), type, searchName), UserModel.countByType(type, searchName)])
            .then(([users, count]) => {
                users.map(v => {
                    delete v.password;
                })
                res.status(200).json({
                    users,
                    total: count[0] ? count[0].total : 0,
                });
            })
            .catch(err => {
                console.log(err);
                res.status(400).json({ message: "failed" });
            })
    }
});

router.post('/lock', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    if (req.body.id == undefined) {
        return res.status(400).json({
            message: "not.enough.field-data"
        });
    }

    var user = {
        id: req.body.id,
        lock: 1,
    };

    UserModel.udpate(user)
        .then((id) => {
            res.status(200).json({
                message: "success",
            });
        })
        .catch(err => {
            console.log(err);
            res.status(400).json({ message: "failed" });
        })
});

router.post('/unlock', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    if (req.body.id == undefined) {
        return res.status(400).json({
            message: "not.enough.field-data"
        });
    }

    var user = {
        id: req.body.id,
        lock: 0,
    };
    UserModel.udpate(user)
        .then((id) => {
            res.status(200).json({
                message: "success",
            });
        })
        .catch(err => {
            console.log(err);
            res.status(400).json({ message: "failed" });
        })
});

router.get('/detail', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var id = req.query.id;
    if (id == undefined || isNaN(id)) {
        return res.status(400).json({ message: "not.enough.field-data" });
    }

    UserModel.findOne(id)
        .then(users => {
            if (users.length > 0) {
                if (users[0].tutorId != null && users[0].tutorId != undefined) {
                    TagSkillModel.queryByUserId(users[0].id)
                    .then(skills => {
                        res.status(200).json({
                            user: users[0],
                            skills
                        });
                    })
                    .catch(err => {
                        console.log(err);
                        res.status(200).json({
                            user: users[0],
                        });
                    });
                } else {
                    res.status(200).json({
                        user: users[0],
                    });
                }
            } else{
                res.status(400).json({ message: "failed" });
            }
        })
        .catch(err => {
            console.log(err);
            res.status(400).json({ message: "failed" });
        })
});

router.post('/ping', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    res.status(200).json({
        role: req.user.role,
    });
});

module.exports = router;