var express = require('express');
var router = express.Router();
const passport = require('passport');
const BenefitModel = require('../../models/benefit/benefit.model');

router.get('/amount', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var groupBy = req.query.groupBy;
    var dateBegin = req.query.dateBegin;
    var dateEnd = req.query.dateEnd;

    if (dateBegin == undefined || dateEnd == undefined
        || dateBegin == "" || dateEnd == "") {
        return res.status(400).json({ message: "not.enough.field-data" });
    }

    if (groupBy == undefined) {
        groupBy = "month";
    }

    BenefitModel.query(dateBegin, dateEnd)
        .then(payments => {

            res.status(200).json({
                complains,
                total: count[0].total,
            });
        })
        .catch(err => {
            console.log(err);
            res.status(400).json({ message: "failed" });
        })
});

const limit = 10;

router.get('/top', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var page = req.query.page;
    var dateBegin = req.query.dateBegin;

    if (page == undefined) {
        page = 1;
    }

    var lm = req.query.limit;
    if (lm == undefined || isNaN(lm)) {
        lm = limit;
    }

    if (dateBegin) {
        var today = new Date().toJSON().slice(0, 10).replace(/-/g, "/");
        Promise.all([BenefitModel.top(dateBegin, today, lm, lm * (page - 1)), BenefitModel.countTop(dateBegin, today)])
            .then(([tops, count]) => {
                res.status(200).json({
                    tops,
                    total: count[0] ? count[0].total : 0,
                });
            })
            .catch(err => {
                console.log(err);
                res.status(400).json({ message: "failed" });
            })
    } else {
        Promise.all([BenefitModel.queryAll(lm, lm * (page - 1)), BenefitModel.countAll()])
            .then(([tops, count]) => {
                res.status(200).json({
                    tops,
                    total: count[0] ? count[0].total : 0,
                });
            })
            .catch(err => {
                console.log(err);
                res.status(400).json({ message: "failed" });
            })
    }

});


router.get('/top-skill', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var page = req.query.page;
    var dateBegin = req.query.dateBegin;

    if (page == undefined) {
        page = 1;
    }

    var lm = req.query.limit;
    if (lm == undefined || isNaN(lm)) {
        lm = limit;
    }

    if (dateBegin) {
        var today = new Date().toJSON().slice(0, 10).replace(/-/g, "/");
        Promise.all([BenefitModel.topBySkill(dateBegin, today, lm, lm * (page - 1)), BenefitModel.countTopBySkill(dateBegin, today)])
            .then(([tops, count]) => {
                res.status(200).json({
                    tops,
                    total: count[0] ? count[0].total : 0,
                });
            })
            .catch(err => {
                console.log(err);
                res.status(400).json({ message: "failed" });
            })
    } else {
        Promise.all([BenefitModel.topBySkillAll(lm, lm * (page - 1)), BenefitModel.countTopBySkillAll()])
            .then(([tops, count]) => {
                res.status(200).json({
                    tops,
                    total: count[0] ? count[0].total : 0,
                });
            })
            .catch(err => {
                console.log(err);
                res.status(400).json({ message: "failed" });
            })
    }

});

module.exports = router;