var express = require('express');
var router = express.Router();
const passport = require('passport');
const bcrypt = require('bcrypt');
const TagSkillModel = require('../../models/tagskill/tagskill.model');

const limit = 10;

router.get('/list', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var page = req.query.page;
    if (page == undefined || isNaN(page)) {
        page = 1;
    }

    var lm = req.query.limit;
    if (lm == undefined || isNaN(lm)) {
        lm = limit;
    }

    Promise.all([TagSkillModel.query(lm, lm * (page - 1)), TagSkillModel.count()])
        .then(([skills, count]) => {
            res.status(200).json({
                skills,
                total: count[0] ? count[0].total : 0,
            });
        })
        .catch(err => {
            console.log(err);
            res.status(400).json({message: "failed"});
        })
});

router.post('/add', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var skill = {};
    skill.title = req.body.title;
    if (skill.title == undefined || skill.title == '') {
        return res.status(400).json({ message: "not.enough.field-data" });
    }
    TagSkillModel.add(skill)
        .then(id => {
            res.status(200).json({ message: "success" });
        })
        .catch(err => {
            console.log(err);
            res.status(400).json({message: "failed"});
        });
});

router.post('/delete', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var skill = {};
    skill.id = req.body.id;
    if (skill.id == undefined) {
        return res.status(400).json({ message: "not.enough.field-data" });
    }
    TagSkillModel.delete(skill)
        .then(id => {
            res.status(200).json({ message: "success" });
        })
        .catch(err => {
            console.log(err);
            res.status(400).json({message: "failed"});
        });
});

module.exports = router;