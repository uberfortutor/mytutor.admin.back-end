var express = require('express');
var router = express.Router();
const passport = require('passport');
const bcrypt = require('bcrypt');
const ManagerModel = require('../../models/manager/manager.model');

const saltRounds = 10;
const limit = 10;

router.get('/list', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var page = req.query.page;
    if (page == undefined || isNaN(page)) {
        page = 1;
    }

    var lm = req.query.limit;
    if (lm == undefined || isNaN(lm)) {
        lm = limit;
    }

    var role = req.query.role;
    if (role == undefined || role == -1 || role == "") {
        Promise.all([ManagerModel.query(lm, lm * (page - 1)), ManagerModel.count()])
            .then(([staffs, count]) => {
                res.status(200).json({
                    staffs,
                    total: count[0] ? count[0].total : 0,
                });
            })
            .catch(err => {
                console.log(err);
                res.status(400).json({ message: "failed" });
            })
    } else {
        Promise.all([ManagerModel.queryByRole(lm, lm * (page - 1), role), ManagerModel.countByRole(role)])
            .then(([staffs, count]) => {
                res.status(200).json({
                    staffs,
                    total: count[0] ? count[0].total : 0,
                });
            })
            .catch(err => {
                console.log(err);
                res.status(400).json({ message: "failed" });
            })
    }
});

router.post('/add', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    if (req.user.role != 0) {
        return res.status(403).json({ message: "Forbidden" });
    }
    var user = {};
    user.username = req.body.username;
    user.password = req.body.password;
    user.name = req.body.name;
    user.role = req.body.role;
    user.email = req.body.email;
    user.delete = 0;
    if (user.username == undefined || user.password == undefined || user.name == undefined || user.email == undefined
        || user.username == '' || user.password == '' || user.name == '' || user.email == '') {
        return res.status(400).json({ message: "not.enough.field-data" });
    }

    user.role = parseInt(user.role);
    if (user.role == undefined) {
        user.role = 1; // staff
    }

    user.password = bcrypt.hashSync(user.password, saltRounds);
    ManagerModel.findOneUsername(user).then(users => {
        if (users.length > 0) {
            return res.status(400).json({ message: "user.already.exist" })
        }
        ManagerModel.add(user).then(id => {
            res.status(200).json({ message: "success" });
        })
            .catch(err => {
                console.log(err);
                res.status(400).json({ message: "failed" });
            });
    })
        .catch(err => {
            console.log(err);
            res.status(400).json({ message: "failed" });
        })
});

router.post('/change-status', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    if (req.user.role != 0) {
        return res.status(403).json({ message: "Forbidden" });
    }
    var user = {};
    user.id = req.body.id;
    user.lock = req.body.status;
    if (user.id == undefined || isNaN(user.id) || user.lock == undefined || isNaN(user.lock)) {
        return res.status(400).json({ message: "not.enough.field-data" });
    }

    ManagerModel.update(user)
        .then((id) => {
            res.status(200).json({ message: "success" });
        })
        .catch(err => {
            console.log(err);
            res.status(400).json({ message: "failed" });
        })
});

router.post('/reset-pass', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    if (req.user.role != 0) {
        return res.status(403).json({ message: "Forbidden" });
    }
    var user = {};
    user.id = req.body.id;
    if (user.id == undefined || isNaN(user.id)) {
        return res.status(400).json({ message: "not.enough.field-data" });
    }
    ManagerModel.findOneId(user).then(users => {
        if (users.length == 0) {
            return res.status(400).json({ message: "user.notfound" })
        }
        user.password = bcrypt.hashSync(users[0].username, saltRounds);
        ManagerModel.update(user)
            .then((id) => {
                res.status(200).json({ message: "success" });
            })
            .catch(err => {
                console.log(err);
                res.status(400).json({ message: "failed" });
            })
    })
        .catch(err => {
            console.log(err);
            res.status(400).json({ message: "failed" });
        })

});

module.exports = router;