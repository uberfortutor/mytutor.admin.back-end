var express = require('express');
var router = express.Router();
const passport = require('passport');
const bcrypt = require('bcrypt');
const ComplainModel = require('../../models/complain/complain.model');
const UserModel = require('../../models/user/user.model');

const limit = 10;

router.get('/list', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var page = req.query.page;
    if (page == undefined || isNaN(page)) {
        page = 1;
    }

    var lm = req.query.limit;
    if (lm == undefined || isNaN(lm)) {
        lm = limit;
    }

    var searchText = req.query.searchText;
    if (searchText == undefined) {
        searchText = "";
    }

    var status = req.query.status;

    if (!status || parseInt(status) === -1) {
        Promise.all([ComplainModel.query(lm, lm * (page - 1), searchText), ComplainModel.count(searchText)])
        .then(([complains, count]) => {
            res.status(200).json({
                complains,
                total: count ? count[0].total : 0,
            });
        })
        .catch(err => {
            console.log(err);
            res.status(400).json({message: "failed"});
        })
    } else {
        Promise.all([ComplainModel.queryBySatus(lm, lm * (page - 1), status, searchText), ComplainModel.countByStatus(status, searchText)])
        .then(([complains, count]) => {
            res.status(200).json({
                complains,
                total: count[0] ? count[0].total : 0,
            });
        })
        .catch(err => {
            console.log(err);
            res.status(400).json({message: "failed"});
        })
    }
});

router.get('/detail', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var id = req.query.id;
    if (id == undefined || isNaN(id)) {
        return res.status(400).json({ message: "not.enough.field-data" });
    }
    
    ComplainModel.detail(id)
    .then(complains=> {
        if (complains.length > 0) {
            const complain = complains[0];
            Promise.all([UserModel.findOne(complain.finderId), UserModel.findOne(complain.tutorId)])
            .then(([finders, tutors]) => {
                console.log(finders, tutors);
                if (finders.length > 0 && tutors.length > 0) {
                    res.status(200).json({
                        complain,
                        finder: finders[0],
                        tutor: tutors[0],
                    });
                } else {
                    res.status(400).json({message: "failed"});
                }
            })
            .catch(err => {
                console.log(err);
                res.status(400).json({message: "failed"});
            });
        } else {
            res.status(400).json({message: "failed"});
        }
    })
    .catch(err => {
        console.log(err);
        res.status(400).json({message: "failed"});
    })
});

router.post('/update-status', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var contract_complain = {};
    contract_complain.id = req.body.id;
    contract_complain.status = req.body.status;
    if (contract_complain.id == undefined || contract_complain.status == undefined) {
        return res.status(400).json({ message: "not.enough.field-data" });
    }
    ComplainModel.udpate(contract_complain)
        .then(id => {
            res.status(200).json({ message: "success" });
        })
        .catch(err => {
            console.log(err);
            res.status(400).json({message: "failed"});
        });
});

module.exports = router;