const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const ManagerModel = require('../models/manager/manager.model');
const passportJWT = require("passport-jwt");
const bcrypt = require('bcrypt');

const secret_key = '`Mb6XB=9{n9RZjh*';
const JWTStrategy = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;


passport.use(new JWTStrategy({
    jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
    secretOrKey: secret_key
},
    function (jwtPayload, cb) {
        //find the user in db if needed. This functionality may be omitted if you store everything you'll need in JWT payload.
        return ManagerModel.findOneUsernameAndRole({ username: jwtPayload.username, role: jwtPayload.role })
            .then(user => {
                if (user.length <= 0) {
                    return cb(null, false);
                }
                else {
                    delete user[0].password;
                    return cb(null, user[0]);
                }
            })
            .catch(err => {
                return cb(err);
            });
    }
));

passport.use(new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password'
},
    function (username, password, cb) {
        console.log(username, password);
        return ManagerModel.findOneUsername({ username })
            .then(user => {
                if (!user || user.length <= 0) {
                    return cb(null, false, { message: 'user.incorrect'  });
                }

                if (bcrypt.compareSync(password, user[0].password) === true) {
                    if (user[0].lock) {
                        return cb(null, false, { message: 'user.locked' });
                    } else {
                        return cb(null, user[0], { message: 'Logged In Successfully' });
                    }
                } else {
                    return cb(null, false, { message: 'user.incorrect' });
                }
            })
            .catch(err =>{
                cb(err, false, { message: 'user.incorrect'  })
                console.log(err);
            } );
    }
));

passport.serializeUser(function (user, done) {
    done(null, user);
});

passport.deserializeUser(function (user, done) {
    done(null, user);
});
