var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors');
const passport = require('passport');
require('./middleware/passport');

var indexRouter = require('./routes/index');
var authenticateRouter = require('./routes/authentication');
var staffRouter = require('./routes/staff');
var tagSkillRouter = require('./routes/tagskill');
var userRouter = require('./routes/user');
var accountRouter = require('./routes/account');
var complainRouter = require('./routes/complain');
var contractRouter = require('./routes/contract');
var benefitRouter = require('./routes/benefit');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
app.use(passport.session());
app.use(cors());

app.use('/users', userRouter);
app.use('/authenticate', authenticateRouter);
app.use('/staff', staffRouter);
app.use('/tagskill', tagSkillRouter);
app.use('/account', accountRouter);
app.use('/complains', complainRouter);
app.use('/contracts', contractRouter);
app.use('/benefit', benefitRouter);
app.use('/', indexRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  console.log(err);
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.json({message: 'error'});
});

app.listen(process.env.PORT || 3000, () => {
  console.log("App is running");
});
